FROM dockcross/windows-static-x86:20191223-c07e40f
MAINTAINER https://gitlab.com/alelec/libwdi_builder

RUN apt-get update && \
  apt-get install -y \
  autoconf \
  autotools-dev \
  dos2unix \
  git \
  p7zip-full

WORKDIR /work

# curl -o wdk10-redist.7z -L http://files.akeo.ie/appveyor/libwdi/wdk10-redist.7z
# curl -o libusb-win32-bin-1.2.6.0.zip -L http://files.akeo.ie/appveyor/libwdi/libusb-win32-bin-1.2.6.0.zip
# curl -o libusbK-3.0.7.0-bin.7z -L http://files.akeo.ie/appveyor/libwdi/libusbK-3.0.7.0-bin.7z

ADD wdk10-redist.7z /work/wdk10-redist.7z
ADD libusb-win32-bin-1.2.6.0.zip /work/libusb-win32-bin-1.2.6.0.zip
ADD libusbK-3.0.7.0-bin.7z /work/libusbK-3.0.7.0-bin.7z

RUN 7z x wdk10-redist.7z && \
    7z x libusb-win32-bin-1.2.6.0.zip && \
    7z x libusbK-3.0.7.0-bin.7z

ENTRYPOINT []
